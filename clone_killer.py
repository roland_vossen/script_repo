#!/usr/bin/env python

"""
This script is a little utility, intended to save space on your hard disk. It finds files with the
same contents and is able to delete the redundant ones. I hope it is useful to more persons than
just me :-)
"""

__author__ = "Roland Vossen"
__copyright__ = "GPL, 2017"
__credits__ = [""]
__license__ = "GPL"
__version__ = "0.1.0"
__maintainer__ = "Roland Vossen"
__email__ = ""
__status__ = "Development"

import os
import argparse
import hashlib

parser = argparse.ArgumentParser(description='Clone Killer V0.1 - gets rid of identical files')
parser.add_argument('dir', metavar='dir', nargs='+', type=str, help='directory to search')
parser.add_argument('--maxdepth', metavar='maxdepth', type=int, help='max subdir level to search (1=no subdirs)', default = -1)
parser.add_argument('--keep', metavar='path_to_keep', type=str, nargs='+', help='don\'t delete file(s) under this full path', default = None)
parser.add_argument('--remove', metavar='path_to_remove', type=str, nargs='+', help='delete file(s) under this full path', default = None)
parser.add_argument('--ignore', metavar='substring_to_ignore', type=str, nargs='+', help='ignore file(s) containing this substring in their full path', default = None)
parser.add_argument('--no_way_back', action="store_true", help='delete duplicate files', default = False)
parser.add_argument('--verbose', action="store_true", help='Prints extra information on what this script is up to')
parser.add_argument('--dups_have_same_fname', action="store_true", help='assumes that differente file names (eg foo.jpg and foo2.jpg) can never be duplicates')

args = parser.parse_args()

a_dict = {} # key: file name. Value: a list of directories where this file resides


def md5(path, chunk_size=1<<22, max_chunks=0):
    """
    Checksums are used to judge if two files are identical. An md5 checksum over a large file is
    a relatively CPU expensive operation. As a part of an optimization, the checksum over the first
    part of a file is calculated in certain cases.
    """
    i_chunk = 0
    hash_md5 = hashlib.md5()
    with open(path, "rb") as f:
        for chunk in iter(lambda: f.read(chunk_size), b""):
            hash_md5.update(chunk)
            i_chunk += 1
            if max_chunks > 0 and i_chunk == max_chunks:
                break # reads only the first part of a file

    return hash_md5.hexdigest()

 
def contains_substring(string_to_test, substrings):
    """
    the --ignore, --keep and --remove options specify substrings with which file paths can be
    filtered
    """
    for substring in substrings:
        if substring in string_to_test:
            return True
    return False


def should_be_ignored(path_to_examine):
    """related to the --ignore option"""
    if args.ignore == None:
        return False

    if contains_substring(path_to_examine, args.ignore):
        if args.verbose:
            print "--ignore: skipping file/dir %s" % path_to_examine
        return True

    return False


# first iteration: create a dictionary of all files that have to be examined
n_files = 0
for a_dir in args.dir:
    a_dir = os.path.normpath(a_dir)
    start_dir_depth = a_dir.count(os.path.sep)
    for root, dirs, files in os.walk(a_dir, topdown=True):
        dir_depth = root.count(os.path.sep)
        if args.maxdepth != -1 and dir_depth - start_dir_depth >= args.maxdepth:
            print "--maxdepth: skipping dir %s..." % root
            dirs[:] = [] # prevents os.walk from recursing deeper in the directory structure
            continue
        if should_be_ignored(root):
            continue # on to the next directory
        print(root)
        for file_name in files:
            path = os.path.join(root, file_name)
            if should_be_ignored(path):
                continue # on to the next file
            fsize = os.path.getsize(path)
            paths = a_dict.get(fsize, None)
            if paths == None:
                paths = [path]
            else:
                paths.append(path)
            a_dict[fsize] = paths
            n_files += 1

# optional iteration: optimization: requiring duplicate files to bare the same name limits the
# amount of files to compare to each other.
if args.dups_have_same_fname:
    another_dict = {}
    for fsize, paths in a_dict.iteritems():
        for path in paths:
            file_name = os.path.basename(path)
            key = (fsize, file_name)
            paths = another_dict.get(key, None)
            if paths == None:
                paths = [path]
            else:
                paths.append(path)
            another_dict[key] = paths
    a_dict = another_dict

print "Analyzing %d files..." % n_files


# next iteration: optimization: gets rid of files without copies, and calculates a 'cheap' checksum
# over each file for comparison purposes. 
another_dict = {}
for key, paths in a_dict.iteritems():
    if len(paths) == 1:
        if args.verbose:
            print "speed optimization: %s has no duplicates" % paths[0]
        continue    
    for path in paths:
        file_name = os.path.basename(path)
        checksum = md5(path, chunk_size=65536, max_chunks=1)        
        new_key = (checksum, key)
        paths = another_dict.get(new_key, None)
        if paths == None:
            paths = [path]
        else:
            paths.append(path)
        another_dict[new_key] = paths
a_dict = another_dict


# next iteration: optimization: gets rid of files without copies, and calculates an 'expensive'
# checksum over each file for comparison purposes. 
another_dict = {}
cumm_fsize = 0
n_checksummed_files = 0
for key, paths in a_dict.iteritems():
    if len(paths) == 1:
        if args.verbose:
            print "speed optimization: %s has no duplicates" % paths[0]
        continue # speed optimization to avoid md5 calculation on non duplicate files

    for path in paths:
        checksum = md5(path)
        paths_with_same_md5 = another_dict.get(checksum, None)
        if paths_with_same_md5 == None:
            paths_with_same_md5 = [path]
        else:
            paths_with_same_md5.append(path)
        another_dict[checksum] = paths_with_same_md5 

    n_checksummed_files += len(paths)
    if n_checksummed_files % 128 == 0:
        # a progress indicator to avoid the user thinking the script has locked up
        print "%d/%d files have been checksummed..." % (n_checksummed_files, n_files)
        cumm_fsize = 0

n_dups = 0
n_deleted = 0


# Iterates over groups of duplicate files, and decides which files to keep or remove.
for checksum, paths in another_dict.iteritems():
    if len(paths) > 1:
        n_dups += len(paths) - 1
        print "duplicates:"
        paths_to_keep = []
        paths_to_remove = []
        error = False
        for path in paths:
            print "    " + path
            if args.keep != None and contains_substring(path, args.keep):
                if args.verbose:
                    print "    --keep: preserving %s" % path
                paths_to_keep.append(path)
            if args.remove != None and contains_substring(path, args.remove):
                if args.verbose:
                    print "    --remove: don't preserve %s" % path
                paths_to_remove.append(path)
            if path in paths_to_keep and path in paths_to_remove:
                print "    Problem: --keep and --remove disagree on %s" % path
                error = True
                break

        if error == True:
            continue

        if len(paths_to_keep) > 1:
            print "    *Don't know which one of these to preserve, use --keep, --remove, --ignore to refine:"
            print "        " + str(paths)
            continue
        
        if len(paths_to_keep) == 0 and len(paths_to_remove) != len(paths) - 1:
            print "    *Don't know which one of these to preserve, use --keep, --remove, --ignore to refine:"
            print "        " + str(paths)
            continue

        for path in paths:
            if path in paths_to_keep:
                continue
            if path not in paths_to_remove:
                if len(paths_to_keep) == 0 and len(paths_to_remove) > 0:
                    continue
            if args.no_way_back == True:
                os.remove(path)
                print "    deleted %s" % path
            else:
                print "    --no_way_back missing: would have deleted %s" % path
            n_deleted += 1

s = "\n%d duplicate files found, %d files " % (n_dups, n_deleted)
if args.no_way_back == True:
    s += "deleted."
else:
    s += "would have been deleted (use --no_way_back)."

print s
